<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>文本预处理</title>
    <jsp:include page="layout/header.jsp"/>
</head>
<body>
<div class="wrap">
    <jsp:include page="layout/nav.jsp"/>
    <div class="container">
        <div class="steps steps-4">
            <ol>
                <li><i>1</i><span class="tsl">文件上传</span></li>
                <li class="active"><i>2</i><span class="tsl">文本预处理</span></li>
                <li><i>3</i><span class="tsl">相似度计算</span></li>
                <li><i>4</i><span class="tsl">聚类展示</span></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <p class="text-danger"></p>

                <p class="text-success"></p>
                <a href="javascript:void(0)" class="btn btn-primary btn-large" onclick="process()" data-toggle="modal"
                   data-target=".bs-example-modal-sm">进行预处理</a>

                <div id="myModal" class="modal bs-example-modal-sm in" tabindex="-1" role="dialog"
                     data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <img src="../../static/images/350.GIF"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="layout/footer.jsp"/>
<script type="text/javascript">
    function process() {
        $.ajax({
            type: "get", //表单提交类型
            url: "/preprocess/segment", //表单提交目标
            dataType: 'json',
            success: function (msg) {
                $('#myModal').modal('hide');
                $(".text-success").text("分词成功，分词总个数：" + msg.wordcount + "；分词总耗时：" + msg.costtime + "毫秒！");
                if (msg == 'success') {//msg 是后台调用action时，你穿过来的参数
                    //do things here
                    window.close();
                } else {
                    //do things here
                }
            }
        });
    }
</script>
</body>
</html>
