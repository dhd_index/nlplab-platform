<%@page pageEncoding="UTF-8" %>
<html>
<head>
    <meta charset="utf-8">
    <title>文件上传</title>
    <jsp:include page="layout/header.jsp"/>
</head>
<body>
<div class="wrap">>
    <jsp:include page="layout/nav.jsp"/>
    <div class="container">
        <div class="steps steps-4">
            <ol>
                <li class="active"><i>1</i><span class="tsl">文件上传</span></li>
                <li><i>2</i><span class="tsl">文本预处理</span></li>
                <li><i>3</i><span class="tsl">相似度计算</span></li>
                <li><i>4</i><span class="tsl">聚类展示</span></li>
            </ol>
        </div>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <p>文件上传要求：******</p>
                <p class="text-danger"></p>
                <p class="text-success"></p>
                <fieldset>
                    <div class="form-group">
                        <input type="file" name="file_upload" id="file_upload"/>
                    </div>
                    <div class="form-group">
                        <label for="comments">文件描述：</label>
                        <textarea id="comments" name="comments" class="form-control"></textarea></div>

                    <input type="button" id="upload" value="上传" class="btn btn-primary btn-large"
                           onclick="$('#file_upload').uploadify('upload', '*')"/>
                    <a href="/preprocess/beforepreprocess" id="processButton" class="btn btn-primary btn-large"
                       style="display: none">进行预处理</a>
                    <input type="button" value="取消" class="btn btn-primary btn-large"
                           onclick="$('#file_upload').uploadify('cancel', '*')"/>
            </div>
            </fieldset>
            <div class="col-md-3"></div>
        </div>
    </div>
</div>
<jsp:include page="layout/footer.jsp"/>
<link rel="stylesheet" type="text/css" href="/static/uploadify/uploadify.css"/>
<script type="text/javascript" src="/static/uploadify/jquery.uploadify.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#file_upload").uploadify({
            'buttonText': '选择文件',
            auto: false,
            height: 30,
            swf: '/static/uploadify/uploadify.swf',
            uploader: '/uploadFiles',
            width: 120,
            'removeTimeout': 1,
            'onQueueComplete': function (queueData) {

                $("#upload").val("继续上传");
                $("#processButton").css('display', 'inline');
                alert(queueData.uploadsSuccessful + '个文件上传成功！');

            },
            'onUploadSuccess': function (file, data, response) {
                var obj = JSON.parse(data);
                if (obj.Code == 0) {
                    $('#file_upload').uploadify('cancel', '*');
                    $(".text-success").text(obj.Msg);
                    $(".text-danger").text(obj.Msg);
                }
            }
        });
    });
</script>
</body>
</html>