<%@page pageEncoding="UTF-8" %>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; NlpLab 2015</p>
        <p class="pull-right">Powered by <a href="http://www.spring.io/" rel="external">SpringMVC Spring</a></p>
    </div>
</footer>
<script type="text/javascript" src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/static/js/utils.js"></script>
